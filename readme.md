# Veracode Sample Scripts for Dynamic Analysis REST API

Various example scripts for Jenkins and GitLab pipelines:

Jenkins:
 - Jenkinsfile - contains 2 stages to call the below scripts
 - jenkins-create-da-scan.py
 - jenkins-update-da-sca.py

 GitLab:
  - .gitlab-ci.yml - contains a security stage to call the below scripts
  - create-da-scan.py
  - update-da-scan.py
  - requirements.txt

